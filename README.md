# The Edgar application core entity objects and classes

## Use cases

Edgar is a source based code coverage tool leveraging Microsoft's Language Server Protocol.

The following use cases for the Edgar application exist:

1) As a User (developer, manager, qa engineer, lawyer), I want to know that all executable statements in a software project are executed by a set of covering programs, like tests, benchmarks, sample apps, etc. Mostly tests. This information can indicate due diligence was used by the team building the project. Or the code is ready to distribute, deploy or release in some fashion. Fully covered means that there is no executable code in the project that was not executed by at least one of the covering programs.

2) A User wants to know which executable statements in a program need to be covered by a test. These indicate areas which the User would be well advised to ensure are covered by a test before releasing that program into the wild. Especially if a failure in that program might cause a plane carrying hundreds of people to crash due to those uncovered statements not being tested, which, of course, happened (see Boeing 737-MAX).

While a fully covered program does not mean the program has no bugs, it does mean the developers have applied the same kind of diligence in achieving a fault-free program that accountants apply to ensuring that the books balance (see Generally Accepted Accounting Principles).

The first use case is valuable. The second use case is more valuable.

## Structure

Edgar has several independently develop-able subprojects and related projects:

1. edgar-app is the main partition and contains the main driving code as well as logging and configuration vertical crosscutting singletons.

2. edgar-instrumenter does the instrumenting on a copy of the target project source code.

3. edgar-data-collector runs the specified covering programs (generally tests) on the instrumented copy of the target project source code to gather a set of coverage data.

4. edgar-report-generator uses the collected coverage data to generate a report for the User.

5. edgar-entities provides the business data classes and objects.

6. lsp4k-core is a Kotlin SDK for the Microsoft Language Server Protocol.

7. lsp4k-coverage-extension is an LSP extension to provide code coverage templates for the Kotlin language. It will serve as a model for other languages leveraging LSP.

8. lsp-test-server is a test language server used by Edgar to drive special casing so that 100% code coverage on Edgar itself is easily achieved.

## Building

Install the deployable (edgar-core-<version>.jar) to the local maven repository using the Gradle task: *publishToMavenLocal* where it will be accessed by Edgar subprojects which depend on the core.
