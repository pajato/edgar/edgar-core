package com.pajato.edgar.core.config

import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject

sealed interface ConfSpec

object ConfSpecSerializer : JsonContentPolymorphicSerializer<ConfSpec>(ConfSpec::class) {
    override fun selectDeserializer(element: JsonElement) = when {
        "id" in element.jsonObject -> EdgarServerSpec.serializer()
        else -> EdgarErrorSpec.serializer()
    }
}
