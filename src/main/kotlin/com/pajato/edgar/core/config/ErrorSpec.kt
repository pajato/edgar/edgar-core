@file:Suppress("Reformat")

package com.pajato.edgar.core.config

import kotlinx.serialization.Serializable

/**
 * An error spec captures the exception information produced when an invalid JSON string is decoded and found to be
 * invalid.
 */
interface ErrorSpec : ConfSpec {
    /**
     * Errors are represented by a diagnostic message. An empty string indicates no error.
     */
    val error: String

    /**
     * The exception message summarizing the cause of the problem. Null if no exception occurred.
     */
    val excMessage: String?

    /**
     * The exception stack trace showing the cause of the problem in the code. Null if no exception occurred.
     */
    val excStackTrace: String?
}

fun ErrorSpec(error: String, excMessage: String? = null, excStackTrace: String? = null): ErrorSpec =
    EdgarErrorSpec(error, excMessage, excStackTrace)

@Serializable internal class EdgarErrorSpec(
    override val error: String,
    override val excMessage: String? = null,
    override val excStackTrace: String? = null,
) : ErrorSpec
