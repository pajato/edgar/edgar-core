@file:Suppress("Reformat")

package com.pajato.edgar.core.config

import kotlinx.serialization.Serializable
import java.net.URI
import java.net.URISyntaxException

/**
 * A server spec provides a configuration for an identified language server. The configuration includes an id, a
 * project URI, and zero or more workspace folder URIs (relative to the project URI). The absence of any folders will
 * result in the default workspace folders of "src/main/<language>" and "src/test/<language>" where <language> might
 * be "kotlin", "java", "scala", "closure", etc. or even empty.
 */
interface ServerSpec : ConfSpec {
    /**
     * The commonly used language server id, e.g. KLS for the Kotlin Language Server developed by (GitHub) @fwcd.
     */
    val id: String

    /**
     * The extension supported by the language server with the given id.
     */
    val extension: String

    /**
     * The project name.
     */
    val projectName: String

    /**
     * A URI specifying the root of the project containing the sources for the coverage analysis.
     */
    val projectUri: String

    /**
     * A URI specifying the root of the instrumented project.
     */
    val copyUri: String

    /**
     * A (possibly empty) list of relative paths to workspace folder roots, e.g. "src/main/kotlin
     */
    val folders: List<String>
}

fun ServerSpec(
    id: String,
    extension: String,
    projectName: String,
    projectUri: String,
    copyUri: String,
    folders: List<String> = listOf(),
): ServerSpec = EdgarServerSpec(id, extension, projectName, projectUri, copyUri, folders)

@Serializable internal class EdgarServerSpec(
    override val id: String,
    override val extension: String,
    override val projectName: String,
    override val projectUri: String,
    override val copyUri: String,
    override val folders: List<String> = listOf(),
) : ServerSpec {
    init {
        require(id.isNotEmpty()) { "The server id must be valid (non-empty)!" }
        require(extension.isNotEmpty()) { "The language extension cannot be empty!" }
        require(projectName.isNotEmpty()) { "The project name cannot be empty!" }
        require(projectUri)
        require(copyUri)
    }

    private fun require(uri: String) {
        fun getDiagnostic(uri: String): String {
            return try { URI(uri); "" } catch (exc: URISyntaxException) { exc.message ?: "No diagnostic available!" }
        }

        val diagnostic = getDiagnostic(uri)
        require(diagnostic.isEmpty()) { "$uri is not a valid URI because: $diagnostic!" }
    }

    override fun toString(): String {
        return """id = "$id", extension = "$extension", projectName = "$projectName", projectUri = "$projectUri", """ +
            """copyUri = "$copyUri", folders = "$folders""""
    }
}
