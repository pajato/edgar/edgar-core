@file:Suppress("Reformat", "KDocMissingDocumentation")

package com.pajato.edgar.core

import com.pajato.edgar.core.config.ConfSpec
import com.pajato.edgar.core.config.ConfSpecSerializer
import com.pajato.edgar.core.config.ErrorSpec
import com.pajato.edgar.core.config.ServerSpec
import kotlinx.serialization.json.Json

/**
 * Add meaning to string objects used to diagnose situations and contexts preventing normal execution.
 */
typealias Diagnostic = String

internal fun isTimedOut(elapsedTime: Long, timeout: Long) = elapsedTime >= timeout

/**
 * Validate a set of arguments by placing each decoded argument into a valid or invalid list and returning both.
 */
fun validateArgs(vararg args: String): Pair<List<ConfSpec>, List<ErrorSpec>> {
    fun convert(spec: String): ConfSpec {
        fun getError(exc: Exception): ErrorSpec =
            ErrorSpec("""Invalid spec: "$spec"""", exc.getMessage(), exc.getStackTrace(spec))

        return try { Json.decodeFromString(ConfSpecSerializer, spec) } catch (exc: Exception) { getError(exc) }
    }

    fun addToCollection(spec: ConfSpec, validItems: MutableList<ConfSpec>, invalidItems: MutableList<ErrorSpec>) =
        when (spec) {
            is ServerSpec -> spec.validate(validItems, invalidItems)
            is ErrorSpec -> invalidItems.add(spec)
        }

    val validItems: MutableList<ConfSpec> = mutableListOf()
    val invalidItems: MutableList<ErrorSpec> = mutableListOf()
    val allItems: List<ConfSpec> = args.map { spec -> convert(spec) }

    allItems.forEach { spec -> addToCollection(spec, validItems, invalidItems) }
    return Pair(validItems, invalidItems)
}

internal fun Exception.getMessage() = this.message ?: "No message available (null)."

internal fun Exception.getStackTrace(spec: String): String {
    val stackTrace = this.stackTrace.joinToString("\n  ")
    return "Stack trace for invalid spec: \"$spec\":\n  $stackTrace"
}

const val EmptyProjectUri: Diagnostic = "The project uri cannot be empty!"
const val SchemeProjectUri: Diagnostic = """The project uri must start with "file:/"!"""
const val PathProjectUri: Diagnostic = "The project uri must have a non-empty path!"
const val fileScheme: String = "file:/"

internal fun ServerSpec.validate(validSpecs: MutableList<ConfSpec>, errorSpecs: MutableList<ErrorSpec>): Boolean {
    fun validateProjectUri(uriErrorSpecs: MutableList<ErrorSpec> = mutableListOf()): Boolean {
        fun hasInvalidPath(uri: String): Boolean = uri.startsWith(fileScheme) && uri.length == fileScheme.length
        fun badSchemeError() = ErrorSpec(error = SchemeProjectUri)
        fun badPathError() = ErrorSpec(error = PathProjectUri)

        if (this.projectUri.isEmpty()) uriErrorSpecs.add(ErrorSpec(error = EmptyProjectUri))
        if (!this.projectUri.startsWith(fileScheme)) uriErrorSpecs.add(badSchemeError())
        if (hasInvalidPath(this.projectUri)) uriErrorSpecs.add(badPathError())
        return if (uriErrorSpecs.isEmpty()) validSpecs.add(this) else errorSpecs.addAll(uriErrorSpecs)
    }

    return validateProjectUri()
}
