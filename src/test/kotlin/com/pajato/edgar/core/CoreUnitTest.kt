package com.pajato.edgar.core

import com.pajato.isaac.core.CoreTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CoreUnitTest : CoreTestProfiler() {

    @Test fun `When a timeout has been exceeded, then verify true is returned`() {
        assertTrue(isTimedOut(100L, 50L))
    }

    @Test fun `When a timeout has not been exceeded, then verify false is returned`() {
        assertFalse(isTimedOut(100L, 200L))
    }

    @Test fun `When a timeout is at the threshold, then verify true is returned`() {
        assertTrue(isTimedOut(100L, 100L))
    }

    @Test fun `When a function returns a diagnostic, verify the correct behavior`() {
        val message = "a helpful diagnostic message..."
        fun validate(): Diagnostic {
            return message
        }

        assertEquals(message, validate())
    }
}
