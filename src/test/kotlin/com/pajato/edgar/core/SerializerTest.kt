package com.pajato.edgar.core

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlin.test.assertEquals

interface SerializerTest {
    fun <T : Any> testEncode(json: String, serializer: JsonContentPolymorphicSerializer<T>, data: T) {
        assertEquals(json, Json.encodeToString(serializer, data))
    }

    fun <T : Any> testDecode(json: String, serializer: JsonContentPolymorphicSerializer<T>, asserts: (T) -> Unit) {
        asserts(Json.decodeFromString(serializer, json))
    }

    /* fun <T : Any> testEncode(json: String, serializer: JsonTransformingSerializer<T>, data: T) {
        assertEquals(json, Json.encodeToString(serializer, data))
    }

    fun <T : Any> testDecode(json: String, serializer: JsonTransformingSerializer<T>, asserts: (T) -> Unit) {
        asserts(Json.decodeFromString(serializer, json))
    }

    fun <T : Any>testEncode(json: String, serializer: KSerializer<T>, data: T) {
        assertEquals(json, Json.encodeToString(serializer, data))
    }

    fun <T : Any>testDecode(json: String, serializer: KSerializer<T>, data: T, asserts: (T) -> Unit) {
        asserts(Json.decodeFromString(serializer, json))
    } */
}
