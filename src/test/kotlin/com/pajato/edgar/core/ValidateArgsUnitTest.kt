package com.pajato.edgar.core

import com.pajato.edgar.core.config.ConfSpec
import com.pajato.edgar.core.config.ConfSpecSerializer
import com.pajato.edgar.core.config.EdgarErrorSpec
import com.pajato.edgar.core.config.EdgarServerSpec
import com.pajato.edgar.core.config.ErrorSpec
import com.pajato.edgar.core.config.ServerSpec
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ValidateArgsUnitTest : SerializerTest {

    private fun getDiagnostic(snippet: String) = "The actual $snippet is wrong!"

    private fun runTest(expected: String, vararg args: String) {
        fun assertValidSpec(valid: List<ConfSpec>, invalid: List<ErrorSpec>) {
            assertTrue(valid.isNotEmpty(), getDiagnostic("valid"))
            assertTrue(invalid.isEmpty(), getDiagnostic("invalid"))
        }

        fun assertInvalidSpec(valid: List<ConfSpec>, invalid: List<ErrorSpec>) {
            assertTrue(valid.isEmpty(), getDiagnostic("valid"))
            assertTrue(invalid.isNotEmpty(), getDiagnostic("invalid"))
            assertEquals(expected, invalid[0].error, getDiagnostic("invalid.error"))
        }

        val (valid, invalid) = validateArgs(*args)
        if (expected.isEmpty()) assertValidSpec(valid, invalid) else assertInvalidSpec(valid, invalid)
    }

    @Test fun `When passed an empty input, verify that validateArgs() returns a correct error spec!`() {
        runTest("""Invalid spec: """"", "")
    }

    @Test fun `When passed a non-empty input, verify validateArgs() returns a correct error spec!`() {
        runTest("""Invalid spec: "xyzzy"""", "xyzzy")
    }

    @Test fun `When passed an empty project URI input, verify validateArgs() returns a correct error spec!`() {
        val input = Json.encodeToString(ConfSpecSerializer, ServerSpec("KLS", "kt", "The Project Name", "", ""))
        runTest("The project uri cannot be empty!", input)
    }

    @Test fun `When passed a spec containing a project URI with an empty scheme, verify result`() {
        val input = Json.encodeToString(ConfSpecSerializer, ServerSpec("KLS", "kt", "A Name", "/abc/xyz", ""))
        runTest("""The project uri must start with "file:/"!""", input)
    }

    @Test fun `When passed a spec containing a project URI with an empty path, verify result`() {
        val input = Json.encodeToString(ConfSpecSerializer, ServerSpec("KLS", "kt", "A Name", "file:/", ""))
        runTest("The project uri must have a non-empty path!", input)
    }

    @Test fun `When accessing the project uri file part of a server spec, verify the result`() {
        val input = ServerSpec("KLS", "kt", "A Name", "file:/abc/xyz", "file:/build/abc/xzy")
        assertEquals("A Name", input.projectName, getDiagnostic("project name"))
    }

    @Test fun `When serializing a server configuration spec, verify the result`() {
        fun assertServerSpec(expected: ServerSpec, actual: ServerSpec) {
            fun assertFolders(expected: Array<String>, actual: Array<String>) {
                assertContentEquals(expected, actual, getDiagnostic("folders"))
            }

            assertTrue(actual is EdgarServerSpec, getDiagnostic("type"))
            assertEquals(expected.id, actual.id, getDiagnostic("id"))
            assertEquals(expected.extension, actual.extension, getDiagnostic("ext"))
            assertFolders(expected.folders.toTypedArray(), actual.folders.toTypedArray())
        }

        val projectUri = System.getProperty("user.dir")
        val copyUri = System.getProperty("user.home")
        val data = ServerSpec("KLS", "kt", "The Project Name", projectUri, copyUri, listOf("src/main/kotlin"))
        val json = """{"id":"KLS","extension":"kt","projectName":"The Project Name","projectUri":"$projectUri",""" +
            """"copyUri":"$copyUri","folders":["src/main/kotlin"]}"""

        testEncode(json, ConfSpecSerializer, data)
        testDecode(json, ConfSpecSerializer) { actual -> assertServerSpec(data, actual as ServerSpec) }
    }

    @Test fun `When serializing an error spec, verify the result`() {
        fun assertErrorSpec(expected: ErrorSpec, actual: ErrorSpec) {
            assertTrue(actual is EdgarErrorSpec, getDiagnostic("type"))
            assertEquals(expected.excMessage, actual.excMessage, getDiagnostic("exception message"))
            assertEquals(expected.excStackTrace!!.length, actual.excStackTrace!!.length, getDiagnostic("stack trace"))
            assertEquals(expected.error, actual.error, getDiagnostic("error"))
        }

        val message = "Element class kotlinx.serialization.json.JsonLiteral ..."
        val stackTrace = "A very long stack trace..."
        val error = "Invalid error: ..."
        val data = ErrorSpec(error, message, stackTrace)
        val json = """{"error":"$error","excMessage":"$message","excStackTrace":"$stackTrace"}"""

        testEncode(json, ConfSpecSerializer, data)
        testDecode(json, ConfSpecSerializer) { actual -> assertErrorSpec(data, actual as ErrorSpec) }
    }

    @Test fun `When converting a server spec to a string, verify the result`() {
        val data = ServerSpec("KLS", "kt", "Some Name", "xyzzy", "abc123", listOf("src/main/c", "src/test/c"))
        val expected = """id = "KLS", extension = "kt", projectName = "Some Name", projectUri = "xyzzy", """ +
            """copyUri = "abc123", folders = "[src/main/c, src/test/c]""""
        assertEquals(expected, data.toString())
    }
}
