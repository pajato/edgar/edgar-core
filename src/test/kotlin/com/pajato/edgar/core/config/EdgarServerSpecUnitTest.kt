package com.pajato.edgar.core.config

import kotlin.test.Test
import kotlin.test.assertFailsWith

class EdgarServerSpecUnitTest {

    @Test fun `When creating a server spec with errors, verify correct behavior`() {
        val scheme = "NoSuchScheme:"
        assertFailsWith<IllegalArgumentException> { ServerSpec("", "", "", "", "") }
        assertFailsWith<IllegalArgumentException> { ServerSpec("xyzzy", "", "", "", "") }
        assertFailsWith<IllegalArgumentException> { ServerSpec("xyzzy", "kt", "", "", "") }
        assertFailsWith<IllegalArgumentException> { ServerSpec("xyzzy", "kt", "fred", scheme, "") }
        assertFailsWith<IllegalArgumentException> { ServerSpec("xyzzy", "kt", "fred", "", scheme) }
    }
}
